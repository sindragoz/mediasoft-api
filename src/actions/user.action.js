import axios from 'axios';
export const loginAction=(login,password)=>dispatch=>{
  console.log(password);
  let postBody={username:login, password:password};
  axios.post('http://localhost:8080/eblank.dev1.msoft.su/api/v1/auth/login',postBody).then(
    response=>{
    dispatch({type:'LOGIN',logged:true, payload:response.data});
  }).catch(err=>{
    dispatch({type:'LOGIN',logged:false, payload:err});
  });
}
export const logoutAction=()=>{
  return {type:'LOGOUT'}
}
export const takeRestaurantsAction=()=>dispatch=>{
  axios.get(`http://localhost:8080/eblank.dev1.msoft.su/api/v1/users/${JSON.parse(localStorage.getItem('user_data')).identity.id}/restaurants-visitor`,
{
        headers: {'Authorization': "Bearer " + JSON.parse(localStorage.getItem('user_data')).auth_key}
   }).then(
    response=>{
    dispatch({type:'TAKE_RESTAURANTS',status:'success', payload:response.data});
  }).catch(err=>{
    dispatch({type:'TAKE_RESTAURANTS',status:'error', payload:err});
  });
}

export const getActionsAction=()=>dispatch=>{
  axios.get(`http://localhost:8080/eblank.dev1.msoft.su/api/v1/actions?expand=evaluations,+restaurant,+responsible_user,+visit_types,+updated_user&filter=%7B%22restaurant_id%22:${JSON.parse(localStorage.getItem('user_data')).identity.department.id}%7D&sort=deadline`,
{
        headers: {'Authorization': "Bearer " + JSON.parse(localStorage.getItem('user_data')).auth_key}
   }).then(
    response=>{
      console.log(response);
    dispatch({type:'TAKE_ACTIONS',status:'success', payload:response.data});
  }).catch(err=>{
    dispatch({type:'TAKE_ACTIONS',status:'error', payload:err});
  });
}

export const changeActionAction=(id,newData)=>{
  console.log(newData);
  return {type:'CHANGE_ACTION',payload:{id,newData}}
}
