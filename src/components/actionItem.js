import React from 'react';
import ReactDOM from 'react-dom';

export const ActionItem=(props)=>(
  <div
    className='actionItem'
    onClick={()=>props.selectItem(props.action.id,props.index)}
  >
    Задача №{props.index+1}<br/>{props.action.deadline}
  </div>
)
