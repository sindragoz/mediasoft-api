import React from 'react';
import ReactDOM from 'react-dom';

export const ActionItemDetails=(props)=>(
  <div
    className='actionItemDetails'
  >
    <h2>Задача №{props.index+1}<span id='redactItem' onClick={()=>props.showRedactModal(true)}>&#9998;</span></h2><br/>
    <span>до</span>: {props.action.deadline}<br/>
    <span>действие</span>: {props.action.description}<br/>
    <span>ответственный</span>: {props.action.responsible_user.name+' '+props.action.responsible_user.surname}<br/>
  </div>
)
