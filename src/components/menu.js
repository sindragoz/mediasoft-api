import React from 'react';
import ReactDOM from 'react-dom';
import IconButton from '@material-ui/core/IconButton';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';

export const Menu=(props)=>(
  <div>
  <div className='menuIcon'>
  <MenuIcon style={{fontSize:'40px',color:'black'}} onClick={()=>props.toggleDrawer(true)} />
  </div>

  <Drawer open={props.showMenu} onClose={()=>props.toggleDrawer(false)}>
          <div className='menu' onClick={()=>props.toggleDrawer(false)}>
            <h2>{props.name+' '+props.surname}</h2>
            <ul style={{margin:'20px',fontSize:'30px', listStyleType:'none',color:'blue',textDecoration:'underline'}}>
              <li>
                <Link to='/visitor/main'>Main page</Link>
              </li>
              <li>
                <Link to='/visitor/restaurants'>Restaurants</Link>
              </li>
              <li>
                <Link to='/visitor/actions'>Actions</Link>
              </li>
            </ul>
            <Button onClick={props.onLogOutHandle} variant="contained" color="secondary" >
              Log out
            </Button>
          </div>
        </Drawer>
          </div>
)
