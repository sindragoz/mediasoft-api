import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
export const NoMatch=(props)=>(
  <div>
  <p style={{textAlign:'center'}}>
  <span style={{fontWeight:'bold',fontSize:'80px'}}>404</span>
  <br/>
  <span style={{fontSize:'50px'}}>Page not found</span><br/>
  <Link to='/' style={{color:'blue'}}>Return to login page</Link>
  </p>
  </div>
)
