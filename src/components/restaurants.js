import React from 'react';
import ReactDOM from 'react-dom';

export const Restaurants=(props)=>(
  <div>
    <div className='Restaurants'>
    <ul>
      {props.restaurants.map(item=>(<li key={item.name_ru}>{item.name_ru}</li>))}
    </ul>
    </div>
  </div>
)
