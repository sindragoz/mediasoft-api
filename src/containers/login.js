import React from 'react';
import ReactDOM from 'react-dom';
import { Redirect } from 'react-router-dom';
import {loginAction, takeDataAction} from '../actions/user.action';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import AccountCircle from '@material-ui/icons/AccountCircle';
import HttpsIcon from '@material-ui/icons/Https';

const iconStyle={lineHeight:'30px', height:'40px',margin:'20px'}


class Login extends React.Component{
  constructor(){
    super();
    this.state={loginValue:'',passValue:'',emptyLoginValue:false,emptyPassValue:false};
  }

  loginChangeHandle(event){
    this.setState({loginValue:event.target.value});
  }

  passChangeHandle(event){
    this.setState({passValue:event.target.value});
  }
  onLoginClickHandle(){
    if(this.validateValues()){
    this.props.loginFunc(this.state.loginValue,this.state.passValue);
  }
    else
    alert('Enter login or password!');
  }
  parseURLParams=()=>{
    if(!this.props.location.search)
    return {};
    let paramStrings=this.props.location.search.split('?')[1].split('&');
    let params={};
    paramStrings.map(item=>{params[item.split('=')[0]]=item.split('=')[1]});
    return params;
  }

  validateValues=()=>{
    this.setState(prevState=>({emptyLoginValue:prevState.loginValue=='',emptyPassValue:prevState.passValue==''}));
    return this.state.loginValue&&this.state.passValue;
  }
  render(){
    //console.log('PROPS');
    //console.log(this.props);
    const urlParams=this.parseURLParams();
    const redirFrom=urlParams.from?urlParams.from:undefined;
    //console.log('USER DATA');
    console.log(localStorage.getItem('user_data'));
    if(localStorage.getItem('user_data')&&this.props.logged)
      if(redirFrom){
        console.log('REDIR!');
        return <Redirect to={redirFrom} />
      }
      else
        return <Redirect to='/visitor/main' />
    return(
      <div>
      <TextField
      error={this.state.emptyLoginValue}
                className='loginInput'
                variant="outlined"
                label="Login"
                value={this.state.loginValue}
                onChange={this.loginChangeHandle.bind(this)}
                onKeyDown={(event)=>{if(event.keyCode===13)this.onLoginClickHandle.bind(this)()}}
                InputProps={{
                  startAdornment: <InputAdornment position="start"><AccountCircle/></InputAdornment>,
                }}
                style={iconStyle}
              /><br/>
              <TextField
              error={this.state.emptyPassValue}
                        className='passInput'
                        variant="outlined"
                        label="Password"
                        type="password"
                        value={this.state.passValue}
                        onChange={this.passChangeHandle.bind(this)}
                        onKeyDown={(event)=>{console.log(event.keyCode);if(event.keyCode===13)this.onLoginClickHandle.bind(this)()}}
                        InputProps={{
                          startAdornment: <InputAdornment position="start"><HttpsIcon/></InputAdornment>,
                        }}
                        style={iconStyle}
                      /><br/>
      <Button className='logBtn' variant="contained" color="primary" onClick={this.onLoginClickHandle.bind(this)}>
        Log In
      </Button>
      </div>
    )
  }
}

function  mapDispatchToProps(dispatch){
  return {
    loginFunc:(login,password)=>dispatch(loginAction(login,password)),
    takeDataFunc:()=>dispatch(takeDataAction())
  }
}

function  mapStateToProps(state){
  return {
     logged:state.loginInfo.logged
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(Login);
