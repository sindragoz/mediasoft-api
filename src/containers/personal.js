import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';
import {Menu} from '../components/menu';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import {Main} from '../components/main';
import VisitorActions from './visitorActions';
import {takeRestaurantsAction,logoutAction, getActionsAction} from '../actions/user.action';
import {Restaurants} from '../components/restaurants';
 class PersonalCabinet extends React.Component{
   constructor(props){
     super(props);
     this.state={showing:true, showMenu:false};
   }
   onLogOutHandle=()=>{
     localStorage.removeItem('user_data');
     this.setState({logout:true});
     this.props.logout();
   }
   toggleDrawer=(show)=>{
     this.setState({showMenu:show});
   }
   componentDidMount(){
     this.props.takeRestaurants();
     this.props.getActions();
   }
 render(){
   console.log('MENU!!!');
   console.log(this.props);
   const userData=JSON.parse(localStorage.getItem('user_data'));
   if(!userData)
   return <Redirect to={{pathname: "/",
  search:`?from=${this.props.location.pathname}`
  }}/>
   console.log(JSON.parse(localStorage.getItem('user_data')));
   const {name,surname}=userData.identity;
   if(this.props.match.params.menuItem=='main')
   return(
    <div>
      <Menu toggleDrawer={this.toggleDrawer} showMenu={this.state.showMenu} name={name} surname={surname} onLogOutHandle={this.onLogOutHandle}/>
      <Main/>
    </div>
)
if(this.props.match.params.menuItem=='restaurants'&&this.props.restaurants)
return(
 <div>
   <Menu toggleDrawer={this.toggleDrawer} showMenu={this.state.showMenu} name={name} surname={surname} onLogOutHandle={this.onLogOutHandle}/>
   <Restaurants restaurants={this.props.restaurants}/>
 </div>
)
if(this.props.match.params.menuItem=='actions'&&this.props.actions)
return(
 <div>
   <Menu toggleDrawer={this.toggleDrawer} showMenu={this.state.showMenu} name={name} surname={surname} onLogOutHandle={this.onLogOutHandle}/>
   <VisitorActions actions={this.props.actions}/>
 </div>
)
return (<div/>)
}
}
function  mapStateToProps(state){
  return {
    restaurants:state.loginInfo.retaurants,
    actions:state.loginInfo.actions
  }
}
function mapDispatchToprops(dispatch){
  return {
    takeRestaurants:()=>dispatch(takeRestaurantsAction()),
    logout:()=>dispatch(logoutAction()),
    getActions:()=>dispatch(getActionsAction())
  }
}

export default connect(mapStateToProps,mapDispatchToprops)(PersonalCabinet);
