import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Calendar from 'react-calendar';
import {changeActionAction} from '../actions/user.action';
const iconStyle={lineHeight:'30px', height:'40px',margin:'20px'}
class RedactAction extends React.Component{
  constructor(props){
    super(props);
    this.state={};
  }
  componentDidMount(){
    console.log('MOUNT');
  console.log(this.props);
    this.setState({descrValue:this.props.action.description,dateValue:this.props.action.deadline});
  }

  dateChangeHandle=(date)=>{
    console.log(date.getMonth());
    this.setState({dateValue:date.getFullYear()+'-'+(date.getMonth()+1)+'-'+date.getDate()});
    //this.setState({dateValue:event.target.value});
  }
  descrChangeHandle(event){
    this.setState({descrValue:event.target.value});
  }
  render() {
    if(this.state.descrValue!=undefined&&this.state.dateValue!=undefined)
    return(
      <div>
      <TextField
                className='descrInput'
                variant="outlined"
                label="Description"
                value={this.state.descrValue}
                onChange={this.descrChangeHandle.bind(this)}
                style={iconStyle}
              /><br/>
        <TextField
        disabled
               className='dataInput'
               variant="outlined"
               label="Deadline"
                        value={this.state.dateValue}
                        style={iconStyle}
                      /><br/>
                      <Calendar
          onChange={this.dateChangeHandle}
          value={new Date(this.state.dateValue)}
        /><br/>
        <Button className='acceptBtn' variant="contained" color="primary" onClick={()=>this.props.changeAction(this.props.action.id,{date:this.state.dateValue,descr:this.state.descrValue})}>
          Accept
        </Button>
      </div>
    )
    return(<div/>);
  }
}

function mapDispatchToProps(dispatch){
  return {
    changeAction:(id,newData)=>dispatch(changeActionAction(id,newData))
  }
}

export default connect((state)=>{return {}},mapDispatchToProps)(RedactAction);
