import React from 'react';
import ReactDOM from 'react-dom';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom';
import 'react-virtualized/styles.css';
import { Column, Table } from 'react-virtualized';
import AutoSizer from 'react-virtualized/dist/commonjs/AutoSizer';
import List from 'react-virtualized/dist/commonjs/List';
import {ActionItem} from '../components/actionItem';
import {ActionItemDetails} from '../components/actionItemDetails';
import Modal from '@material-ui/core/Modal';
import RedactAction from './redactAction';
import {changeActionAction} from '../actions/user.action';

const modalStyle = {
  position: 'fixed',
  zIndex: 1040,
  top: 0, bottom: 0, left: 0, right: 0
};
const backdropStyle = {
  ...modalStyle,
  zIndex: 'auto',
  backgroundColor: '#000',
  opacity: 0.5
};
const dialogStyle = {
    position: 'absolute',
    width: 400,
    top: '50%', left: '50%',
    transform: `translate(-50%, -50%)`,
    border: '1px solid #e5e5e5',
    backgroundColor: 'white',
    boxShadow: '0 5px 15px rgba(0,0,0,.5)',
    padding: 20
};
export default class VisitorActions extends React.Component{
  constructor(props){
    super(props);
    this.state={};
  }
  componentDidMount(){
    this.setState({currentItemId:this.props.actions[0].id,currentItemIndex:0});
  }
  rowRenderer=({
    key,         // Unique key within array of rows
    index,       // Index of row within collection
    isScrolling, // The List is currently being scrolled
    isVisible,   // This row is visible within the List (eg it is not an overscanned row)
    style       // Style object to be applied to row (to position it)
  })=> {
    return (
      <div style={style} key={key} className={'actionItemlist'}>
      <ActionItem
        selectItem={this.selectItem.bind(this)}
        action={this.props.actions[index]}
        index={index}

      />
      </div>
    )
  }
  selectItem(id,index){
    this.setState({currentItemId:id,currentItemIndex:index});
  }
  showRedactModal=(show)=>{
    this.setState({redactShowed:show});
  }
  render() {
    const action=this.props.actions.find(item=>item.id===this.state.currentItemId);
    if(this.state.currentItemId)
    return(
      <div>
      <List
  width={300}
  height={500}
  rowCount={this.props.actions.length}
  rowHeight={50}
  style={{display:'inline-block',float:'left'}}
  rowRenderer={this.rowRenderer}
/>
<ActionItemDetails showRedactModal={this.showRedactModal} index={this.state.currentItemIndex} action={action}/>
<Modal
          aria-labelledby='modal-label'
          style={modalStyle}
          backdropstyle={backdropStyle}
          show={this.state.showModal}
          onClose={()=>this.showRedactModal(false)}
          open={!!this.state.redactShowed}
        >
          <div style={dialogStyle} >
            <RedactAction action={action}/>

          </div>
        </Modal>
      </div>
    )
    return(<div/>);
  }
}
