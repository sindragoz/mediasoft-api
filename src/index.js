import React from 'react';
import ReactDOM from 'react-dom';
import { Switch } from 'react-router';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from './containers/login';
import PersonalCabinet from './containers/personal';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import allReducers from './reducers';
import { Redirect } from 'react-router-dom';
import {NoMatch} from './components/nomatch';
import './style.css';


const middleware=applyMiddleware(thunk);
const store=createStore(allReducers,middleware);


const ComponentRouter=()=>(
<Router>
	<div>
	<Switch>
  <Route exact path="/" component={Login} />
	<Route path="/visitor/:menuItem" component={PersonalCabinet} />
	<Route component={NoMatch}/>
	</Switch>

	</div>
</Router>
);

const App=()=>(

	<div id='AppContainer'>
		<ComponentRouter/>
	</div>
	);


ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>
  ,
  document.getElementById('app')
);
