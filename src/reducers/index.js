import {combineReducers} from 'redux';
import {loginInfo} from './login';

const allReducers=combineReducers({
  loginInfo
});

export default allReducers;
